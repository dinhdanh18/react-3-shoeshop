import React, { Component } from 'react'

export default class CartShoeShop extends Component {

  renderCart = () => {
    return this.props.cart.map((item) => { 
      return <tr style={{fontWeight : "500",height : "70px" , lineHeight : "70px"}} key={item.id}>
        <td>{item.id}</td>
        <td><img src={item.image} alt="" style={{width : "70px",height : "70px"}} /></td>
        <td>{item.name}</td>
        <td>{item.price}</td>
        <td>
          <button onClick={() => {this.props.handleChangeQuantity(item.id,-1)}} className='btn btn-primary mx-2'>-</button>
          {item.soluong}
          <button onClick={() => { this.props.handleChangeQuantity(item.id,1) }} className='btn btn-primary mx-2'>+</button>
        </td>
        <td>
          <button onClick={() => { 
            this.props.handleRemoveShoe(item.id)
           }} className='btn btn-danger'>Delete</button>
        </td>
      </tr>
    })
  }
  render() {
    return (
      <div className='p-1'style={{position:"fixed",left:"50%",transform:"translateX(-50%)",zIndex : "3",background : "#fff",borderRadius:"10px"}}>
        <table className='table my-3'>
          <thead>
            <tr>
              <th>ID</th>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          {this.renderCart()}
          </tbody>
        </table>
      </div>
    )
  }
}
