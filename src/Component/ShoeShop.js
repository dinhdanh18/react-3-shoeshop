import React, { Component } from "react";
import CartShoeShop from "./CartShoeShop";
import { shoeArr } from "./data_shoeShop";
import ItemShoeShop from "./ItemShoeShop";

export default class ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    cart: [],
  };
  renderShoe = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoeShop
          hanndelAddToCard={this.hanndelAddToCard}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };
  hanndelAddToCard = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneCart = [...this.state.cart];
    if (index == -1) {
      let newShoe = { ...shoe, soluong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soluong++;
    }
    this.setState({ cart: cloneCart });
  };
  handleRemoveShoe = (idShoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let cloneCart = [...this.state.cart];
      cloneCart.splice(index, 1);
      this.setState({ cart: cloneCart });
    }
  };
  handleChangeQuantity = (idShoe,step) => {
    let index = this.state.cart.findIndex((item) => { 
      return item.id == idShoe;
     })
    let cloneCart  = [...this.state.cart];
    cloneCart[index].soluong += step;
    if(cloneCart[index].soluong == 0 ){
      cloneCart.splice(index,1);
    }
    this.setState({cart:cloneCart});
  }
  
  render() {
    return (
      <div style={{background:"#ecf0f1"}}>
      <div className="container" >
        <h1 className="text-info">SHOE SHOP</h1>
        <p className="text-danger font-weight-bold">Cart ({this.state.cart.length})</p>
        {this.state.cart.length > 0 && <CartShoeShop 
        handleRemoveShoe = {this.handleRemoveShoe}
        handleChangeQuantity = {this.handleChangeQuantity}
        cart={this.state.cart} />}
        <div className="row">{this.renderShoe()}</div>
      </div>
      </div>
    );
  }
}
