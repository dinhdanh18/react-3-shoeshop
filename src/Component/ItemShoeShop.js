import React, { Component } from "react";

export default class ItemShoeShop extends Component {
    render() {
        let { image, name, description } = this.props.shoeData;
        return (
            <div className="col-3 my-3">
                <div className="card"style={{background:"#bdc3c7"}} >
                    <img className="card-img-top" src={image} alt="Card image cap" />
                    <div className="card-body">
                        <h5 className="card-title">
                            {name.lenght < 20 ? name : name.slice(0, 20) + "..."}
                        </h5>
                        <p className="card-text">
                            {description.lenght < 30
                                ? description
                                : description.slice(0, 30) + "..."}
                        </p>
                        <button onClick={() => { this.props.hanndelAddToCard(this.props.shoeData) }} className="btn btn-success">
                            Add to cart
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
